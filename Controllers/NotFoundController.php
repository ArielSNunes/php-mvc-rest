<?php
namespace Controllers;

use Core\Controller;


class NotFoundController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->sendJson(403, array('msg' => 'Controller de Erro Alcançado'));
	}
}
