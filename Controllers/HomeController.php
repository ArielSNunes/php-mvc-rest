<?php
namespace Controllers;

use Core\Controller;

class HomeController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$arr = array('nome' => 'Ariel', 'idade' => 24);
		$this->sendJson(200, $arr);
	}
	public function listar($id)
	{
		$arr = array('nome' => 'Ariel', 'idade' => 24, 'id' => $id);
		$this->sendJson(200, $arr);
	}
}
