<?php
date_default_timezone_set('America/Sao_Paulo');
require('env.php');

$config = array();
if (ENV === 'D') {
	define('BASE_URL', 'http://localhost:8080/pessoais/rest-mvc');
	$config['dbtype'] = 'mysql';
	$config['host'] = '127.0.0.1';
	$config['port'] = '3306';
	$config['user'] = 'ArielSNunes';
	$config['pass'] = 'Ariel';
	$config['dbname'] = 'test';
	$config['jwt_secret_key'] = 'aBcDeF135444!.??';
} else {
	define('BASE_URL', 'http://www.meusite.com.br');
	$config['dbtype'] = 'mysql';
	$config['host'] = '192.168.0.1';
	$config['port'] = '3306';
	$config['user'] = 'root';
	$config['pass'] = 'root';
	$config['dbname'] = 'estrutura_mvc';
	$config['jwt_secret_key'] = 'aBcDeF135444!.??';
}

try {
	global $db;
	$db = new PDO(
		$config['dbtype'] . ':host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['dbname'],
		$config['user'],
		$config['pass'],
		array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_EMULATE_PREPARES => true,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		)
	);
} catch (PDOException $err) {
	Utils\StatusCodes::send(400);
	header('Content-Type: application/json; charset=utf-8');
	echo (json_encode(Utils\DBErrors::getError($err)));
	exit;
}
