<?php
namespace Utils;

class DBErrors
{
	private static function formatMsg($statusCode, $msg)
	{
		return array('errorCode' => $statusCode, 'msg' => $msg, 'status' => 'ERR');
	}
	private static function err1036($msg)
	{
		return self::formatMsg(1036, 'Tabela é somente leitura');
	}
	private static function err1040($msg)
	{
		return self::formatMsg(1036, 'Muitas conexões');
	}
	private static function err1042($msg)
	{
		return self::formatMsg(1042, 'Erro ao acessar o Host');
	}
	private static function err1044($msg)
	{
		return self::formatMsg(1044, 'Acesso negado ao banco para o usuário');
	}
	private static function err1045($msg)
	{
		return self::formatMsg(1045, 'Acesso negado ao banco para o usuário (usando senha)');
	}
	private static function err1048($msg)
	{
		return self::formatMsg(1048, 'Coluna {' . $msg . '} não pode ser nulo');
	}
	private static function err1054($msg)
	{
		$key = explode(' ', $msg);
		$key = str_replace('\'', '', $key[2]);
		return self::formatMsg(1054, 'Campo {' . $key . '} desconhecido');
	}
	private static function err1062($msg)
	{
		$key = explode(' ', $msg);
		$key = array_pop($msg);
		$key = str_replace('\'', '', $key);
		return self::formatMsg(1062, 'Dado do campo {' . $key . '} já existe no banco');
	}
	private static function err23000($msg)
	{
		$key = explode(' ', $msg);
		$key = array_pop($key);
		$key = str_replace('\'', '', $key);
		return self::formatMsg(23000, 'Dado do campo {' . $key . '} já existe no banco');
	}
	private static function err2002($msg)
	{
		return self::formatMsg(2002, 'Conexão recusada');
	}
	public static function getError($err)
	{
		$errorCode = preg_match('/\d{4}/', $err->getCode()) ? $err->getCode() : $err->errorInfo[1];
		$errorMessage = $err->errorInfo[2];
		$errorCode = 'err' . $errorCode;
		return self::$errorCode($errorMessage);
	}
}
