<?php
namespace Utils;

class Jwt
{
	public static function create($data)
	{
		global $config;
		$header = json_encode(array("typ" => "JWT", "alg" => "HS256"));
		$payload = json_encode($data);
		$encodedHeader = $this->base64url_encode($header);
		$encodedPayload = $this->base64url_encode($payload);
		$signature = hash_hmac("sha256", $encodedHeader . "." . $encodedPayload, $config['jwt_secret_key'], true);
		$encodedSignature = $this->base64url_encode($signature);
		$jwt = $encodedHeader . "." . $encodedPayload . "." . $encodedSignature;
		return $jwt;
	}

	public static function validate($jwt)
	{
		global $config;
		$array = array();
		$jwt_splits = explode('.', $jwt);
		if (count($jwt_splits) == 3) {
			$signature = hash_hmac("sha256", $jwt_splits[0] . "." . $jwt_splits[1], $config['jwt_secret_key'], true);
			$encodedSignature = $this->base64url_encode($signature);
			if ($encodedSignature == $jwt_splits[2]) {
				$array = json_decode($this->base64url_decode($jwt_splits[1]));
			}
		}
		return $array;
	}

	private static function base64url_encode($data)
	{
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	private static function base64url_decode($data)
	{
		return base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($data)) % 4));
	}
}
