<?php
session_start();

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

if (file_exists('vendor/autoload.php'))
	require('vendor/autoload.php');
require('config.php');
require('routes.php');

$core = new Core\Core();
$core->run();